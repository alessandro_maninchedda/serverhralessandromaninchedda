const db = require("../models");
const Employee = db.employees;

// Create and Save a new Employee
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create an Employee
  const employee = new Employee({
    id_employee: req.body.id_employee,
    name: req.body.name,
    picture: req.body.picture,
    phone_number: req.body.phone_number,
    email: req.body.email,
    hired_date: req.body.hired_date,
    manager_id: req.body.manager_id,
  });

  // Save Employee in the database
  employee
    .save(employee)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "An error occurred while creating the employee."
      });
    });
};

// Retrieve all employees from the database.
exports.findAll = (req, res) => {
  const id_employee = req.query.id_employee;
  var condition = id_employee ? { id_employee: { $regex: new RegExp(id_employee), $options: "i" } } : {};

  Tutorial.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving employees."
      });
    });
};

// Find a single employee with an id
exports.findOne = (req, res) => {
  const id_employee = req.params.id_employee;

  Employee.findById(id_employee)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found employee with id " + id_employee });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving employee with id=" + id_employee });
    });
};

// Update an employee by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id_employee = req.params.id_employee;

  Employee.findByIdAndUpdate(id_employee, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update employee with id=${id_employee}. Maybe Employee was not found!`
        });
      } else res.send({ message: "Employee was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating employee with id=" + id
      });
    });
};

// Delete an employee with the specified id in the request
exports.delete = (req, res) => {
  const id_employee = req.params.id_employee;

  Employee.findByIdAndRemove(id_employee, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete employee with id=${id_employee}. Maybe Employee was not found!`
        });
      } else {
        res.send({
          message: "Employee was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete employee with id=" + id_employee
      });
    });
};

// Delete all employees from the database.
exports.deleteAll = (req, res) => {
  Employee.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Employees were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all employees."
      });
    });
};

