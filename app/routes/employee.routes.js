module.exports = app => {
  const employees = require("../controllers/employee.controller.js");

  var router = require("express").Router();

  // Create a new Employee
  router.post("/", employees.create);

  // Retrieve all employees
  router.get("/", employees.findAll);

  // Retrieve all published employees
  router.get("/published", employees.findAllPublished);

  // Retrieve a single Employee with id
  router.get("/:id", employees.findOne);

  // Update an employee with id
  router.put("/:id", employees.update);

  // Delete an employee with id
  router.delete("/:id", employees.delete);

  // Create a new employee
  router.delete("/", employees.deleteAll);

  app.use("/api/employees", router);
};
